﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableItem : MonoBehaviour {

    public Animator spriteAnimator;
    public int health;
    public GameObject itemDrop;
    private int maxHealth;

    private int waitToDie;

    public SpriteRenderer sprite;
	// Use this for initialization
	void Start() {
        maxHealth = health;
        waitToDie = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(waitToDie > 0)
        {
            waitToDie--;
            if(waitToDie <= 0)
            {
                int r = Random.Range(1, 101);
                if (r <= 50)
                {
                    GameObject item = Instantiate(itemDrop) as GameObject;
                    item.transform.position = this.transform.position;
                    item.GetComponent<SpriteRenderer>().sortingOrder = -(int)(item.transform.position.y * 100);
                }
                Destroy(gameObject);
            }
        }
	}

    void LateUpdate()
    {
        sprite.sortingOrder = -(int)(transform.position.y * 100);
    }

    public void DealDamage(int damage)
    {
        health -= damage;
        if (waitToDie == 0) {
            if (health <= (maxHealth / 2))
            {
                spriteAnimator.Play("Hurt", -1);
            }
            if (health <= 0)
            {
                spriteAnimator.Play("Broken", -1);
                waitToDie = 15;
            }
        }
    }
}
