﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatMovement : MonoBehaviour {

    private float Right;

    public int waitTimer = 0;

	// Use this for initialization
	void Start () {
        Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;
    }
	
	// Update is called once per frame
	void Update () {
        if(waitTimer <= 40)
        {
            waitTimer++;
        }

        if (waitTimer >= 40)
        {
            this.transform.position = new Vector3(this.transform.position.x + 0.4f, this.transform.position.y, this.transform.position.z);
        }

        if (gameObject.activeSelf == true && waitTimer >= 40)
        {
            if (Camera.current != null)
            {
                Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;
                
                if (transform.position.x + gameObject.GetComponent<BoxCollider2D>().offset.x >= Right)
                {
                    this.transform.localPosition = new Vector3(-6.8f, this.transform.localPosition.y, this.transform.localPosition.z);
                    waitTimer = 0;
                    gameObject.SetActive(false);
                    
                }
            }
        }
    }
}
