﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaveSpecialAttack : MonoBehaviour {

    public GameObject[] phrases;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PickPhrase()
    {
        for (int i = 0; i < phrases.Length; i++)
        {
            phrases[i].SetActive(false);
        }

        int index = Random.Range(0, phrases.Length);
        phrases[index].SetActive(true);
        phrases[index].GetComponent<SpriteRenderer>().sortingOrder = gameObject.GetComponent<SpriteRenderer>().sortingOrder + 1;
    }

    public void AdjustWordsOffset()
    {
        for(int i = 0; i < phrases.Length; i++)
        {
            phrases[i].transform.localPosition = new Vector3(phrases[i].transform.localPosition.x * -1.0f, 
                phrases[i].transform.localPosition.y, phrases[i].transform.localPosition.z);
        }
    }
}
