﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public GameStateManager gameManager;
    public GameObject HealthBar, SuperBar;

    public GameObject Rats;

    public bool player1, player2, player3, player4;
    public float MaxSpeed;
    public float speed;             //Floating point variable to store the player's movement speed.
    private AttackActions attack;

    private float Left, Right, Bottom;
    private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.
    private bool screenMoving;

    private bool facingRight, isMoving;

    public SpriteRenderer sprite;
    public Animator spriteAnimator;

    private int objectHorizontal; //-1 if object to the left, 1 if to the right
    private int objectVertical; //-1 if object below, 1 if object above

    private string LA, HA, LeftRight, UpDown, UpDown2, LeftRight2, Pause;

    private int meter;

    private HitHandler hitHandler;

    private bool healthItemHit;

    // Use this for initialization
    void Start()
    {
        isMoving = false;

        if (player1)
        {
            LeftRight = "Horizontal";
            UpDown = "Vertical";
            LA = "LA";
            HA = "HA";
            UpDown2 = "ControllerVertical";
            LeftRight2 = "ControllerHorizontal";
            Pause = "Pause1";
        }
        else if (player2)
        {
            LeftRight = "Horizontal2";
            UpDown = "Vertical2";
            LA = "LA2";
            HA = "HA2";
            UpDown2 = "ControllerVertical2";
            LeftRight2 = "ControllerHorizontal2";
            Pause = "Pause2";
        }
        else if (player3)
        {
            LeftRight = "ControllerHorizontal3";
            UpDown = "ControllerVertical3";
            LA = "LA3";
            HA = "HA3";
            UpDown2 = "ControllerVertical3";
            LeftRight2 = "ControllerHorizontal3";
            Pause = "Pause3";
        }
        else if (player4)
        {
            LeftRight = "ControllerHorizontal4";
            UpDown = "ControllerVertical4";
            LA = "LA4";
            HA = "HA4";
            UpDown2 = "ControllerVertical4";
            LeftRight2 = "ControllerHorizontal4";
            Pause = "Pause4";
        }
        attack = gameObject.GetComponent<AttackActions>();

        //Get and store a reference to the Rigidbody2D component so that we can access it.
        speed = 2.0f;
        MaxSpeed = 2.0f;
        rb2d = GetComponent<Rigidbody2D>();

        Left = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x + 0.5f;
        Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;
        Bottom = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y + 1.0f;
        screenMoving = false;

        facingRight = true;

        objectHorizontal = 0;
        objectVertical = 0;

        meter = 100;

        hitHandler = gameObject.GetComponent<HitHandler>();

        healthItemHit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!hitHandler.IsStunned())
        {
            if (Input.GetButton(LA) && meter >= 100)
            {
                if (Input.GetButton(HA))
                {
                    bool super = attack.SAttack();
                    if (super)
                    {
                        gameManager.PlaySuper();
                        meter -= 100;
                        AdjustSuper();
                        if(player2 || player4)
                        {
                            if(Rats != null)
                            {
                                Rats.SetActive(true);
                            }
                        }
                    }
                }
            }


            if (Input.GetButtonDown(LA))
            {
                attack.LAttack();
            }

            if (Input.GetButtonDown(HA))
            {
                if ((Input.GetAxis(UpDown) > 0 || Input.GetAxis(UpDown2) > 0) && meter >= 25)
                {
                    bool special = attack.SpAttack();
                    if (special)
                    {
                        meter -= 25;
                        AdjustSuper();
                    }
                }
                else if ((Input.GetAxis(UpDown) < 0 || Input.GetAxis(UpDown2) < 0) && gameManager.CanCallAssist() /*&& meter >= 50*/)
                {
                    bool assist = gameManager.PerformAssist();
                    /*if (assist)
                    {
                        meter -= 50;
                        AdjustSuper();
                    }*/
                }
                else
                {
                    attack.HAttack();
                }
            }

            if (Input.GetButtonDown(Pause))
            {
                if (Time.timeScale == 0.0f)
                    Time.timeScale = 1.0f;
                else
                {
                    Time.timeScale = 0.0f;
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            if (Input.GetKeyDown(KeyCode.Backspace) && Time.timeScale == 0.0f)
            {
                Time.timeScale = 1.0f;
                SceneManager.LoadScene("TitleScreen");
            }

            if (isMoving && !attack.IsAttacking())
            {
                spriteAnimator.Play("Walk", -1);
            }

            if (!isMoving && !attack.IsAttacking())
            {
                spriteAnimator.Play("Idle", -1);
            }
        }

        if (hitHandler.IsStunned())
        {
            if(!hitHandler.isDown())
                spriteAnimator.Play("Hit", -1);
            else if (hitHandler.isDown())
                spriteAnimator.Play("Down", -1);

            if(transform.position.x <= Left || transform.position.x >= Right)
            {
                rb2d.velocity = new Vector2(0.0f, rb2d.velocity.y);
            }

            if (transform.position.y <= Bottom)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0.0f);
            }

            
        }
    }

    void FixedUpdate()
    {
        Left = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x + 0.5f;
        Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;

        /*if (screenMoving)
        {
            Left = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x + 0.5f;
            Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;
        }*/


        if (!attack.IsAttacking() && !hitHandler.IsStunned()) { 
            //Store the current horizontal input in the float moveHorizontal.
            float moveHorizontal = Input.GetAxis(LeftRight);
            if(moveHorizontal == 0)
                moveHorizontal = Input.GetAxis(LeftRight2);

            //Store the current vertical input in the float moveVertical.
            float moveVertical = Input.GetAxis(UpDown);
            if (moveVertical == 0)
                moveVertical = Input.GetAxis(UpDown2);

            if (moveHorizontal == 0 && moveVertical == 0)
            {
                isMoving = false;
            }
            else {
                isMoving = true;
            }

            if ((facingRight && moveHorizontal < 0.0f) || (!facingRight && moveHorizontal > 0.0f))
            {
                //Flip player
                sprite.transform.localPosition = new Vector3(sprite.transform.localPosition.x * -1.0f, 
                    sprite.transform.localPosition.y, sprite.transform.localPosition.z);
                sprite.flipX = !sprite.flipX;
                facingRight = !facingRight;
                attack.FlipAttacks();
            }

            if ((transform.position.y >= 0.0f && moveVertical > 0.0f) || 
                (transform.position.y <= Bottom && moveVertical < 0.0f) ||
                (objectVertical == 1 && moveVertical > 0.0f) ||
                (objectVertical == -1 && moveVertical < 0.0f))
            {
                moveVertical = 0.0f;
            }

            /*if (transform.position.y <= Bottom && moveVertical < 0.0f)
            {
                moveVertical = 0.0f;
            }*/

            if ((transform.position.x <= Left && moveHorizontal < 0.0f) ||
                (transform.position.x >= Right && moveHorizontal > 0.0f /*&& !screenMoving*/) ||
                (objectHorizontal == 1 && moveHorizontal > 0.0f) ||
                (objectHorizontal == -1 && moveHorizontal < 0.0f))
            {
                moveHorizontal = 0.0f;
            }

            /*if (transform.position.x >= Right && moveHorizontal > 0.0f && !screenMoving)
            {
                moveHorizontal = 0.0f;
            }*/

            //Use the two store floats to create a new Vector2 variable movement.
            //Vector2 movement = new Vector2(moveHorizontal, moveVertical);

            //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
            rb2d.velocity = new Vector2(moveHorizontal * MaxSpeed, moveVertical * MaxSpeed);
        }
        else if (attack.IsAttacking())
        {
            float moveHorizontal = 0.0f;
            float moveVertical = 0.0f;
            //Vector2 movement = new Vector2(moveHorizontal, moveVertical);

            rb2d.velocity = new Vector2(moveHorizontal * MaxSpeed, moveVertical * MaxSpeed);
        }
        else if (hitHandler.IsStunned() && rb2d.gravityScale == 0)
        {
            float moveHorizontal = 0.0f;
            float moveVertical = 0.0f;
            Vector2 movement = new Vector2(moveHorizontal, moveVertical);
            rb2d.velocity = new Vector2(moveHorizontal * MaxSpeed, moveVertical * MaxSpeed);
        }
    }

    void LateUpdate()
    {
        sprite.sortingOrder = -(int)(transform.position.y * 100);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Player")
        {
            rb2d.velocity = new Vector2(0.0f, 0.0f);
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);

            Vector2 point = collision.contacts[0].point; //point of collision
            //Vector2 center = collision.collider.bounds.center; //center of other object
            Vector2 min = collision.collider.bounds.min;
            Vector2 max = collision.collider.bounds.max;

            if (point.x <= min.x)
            {
                objectHorizontal = 1;
            }
            else if (point.x >= max.x)
            {
                objectHorizontal = -1;
            }

            if (point.y <= min.y)
            {
                objectVertical = 1;
            }
            else if (point.y >= max.y)
            {
                objectVertical = -1;
            }

            /*if (point.x < center.x)
            {
                objectHorizontal = 1;
            }
            else if (point.x > center.x)
            {
                objectHorizontal = -1;
            }

            if (point.y < center.y)
            {
                objectVertical = 1;
            
            else if (point.y > center.y)
            {
                objectVertical = -1;
            }*/

            /*if (collision.gameObject.transform.position.x > transform.position.x)
            {
                objectHorizontal = 1;
            }
            else if (collision.gameObject.transform.position.x < transform.position.x)
            {
                objectHorizontal = -1;
            }

            if (collision.gameObject.transform.position.y > transform.position.y)
            {
                objectVertical = 1;
            }
            else if (collision.gameObject.transform.position.y < transform.position.y)
            {
                objectVertical = -1;
            }*/
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Player")
        {
            objectVertical = 0;
            objectHorizontal = 0;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "HealthItem" && !healthItemHit)
        {
            healthItemHit = true;
            gameObject.GetComponent<AudioSource>().Play();
            Destroy(collision.gameObject);
            hitHandler.AddHealth(20);
            AdjustHealth();
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HealthItem" && healthItemHit)
        {
            healthItemHit = false;
        }
    }

    public void SetScreenMoving(bool moving)
    {
        screenMoving = moving;
    }

    public bool IsFacingRight()
    {
        return facingRight;
    }

    public void FlipPlayer()
    {
        sprite.transform.localPosition = new Vector3(sprite.transform.localPosition.x * -1.0f,
                    sprite.transform.localPosition.y, sprite.transform.localPosition.z);
        sprite.flipX = !sprite.flipX;
        facingRight = !facingRight;
        attack.FlipAttacks();
    }

    public void AdjustHealth()
    {
        int health = hitHandler.GetHealth();
        Vector3 pos = HealthBar.transform.localPosition;
        float width = ((RectTransform)HealthBar.transform).rect.width;
        if (health <= 0)
        {
            HealthBar.transform.localPosition = new Vector3(-width, pos.y, pos.z);
            gameManager.PlayGameOver();
        }
        else if (health >= 100)
        {
            HealthBar.transform.localPosition = new Vector3(0.0f, pos.y, pos.z);
        }
        else
        {
            float newX = -width - (-width * health / 100.0f);
            HealthBar.transform.localPosition = new Vector3(newX, pos.y, pos.z);
        }
    }

    public void AdjustSuper()
    {
        Vector3 pos = SuperBar.transform.localPosition;
        float width = ((RectTransform)SuperBar.transform).rect.width;
        //float xPos = SuperBar.transform.localPosition.x;
        if (meter <= 0)
        {
            SuperBar.transform.localPosition = new Vector3(-width, pos.y, pos.z);
            meter = 0;
        }
        else if (meter >= 100)
        {
            SuperBar.transform.localPosition = new Vector3(0.0f, pos.y, pos.z);
            meter = 100;
        }
        else
        {
            float newX = -width - (-width * meter/100.0f);
            SuperBar.transform.localPosition = new Vector3(newX, pos.y, pos.z);
        }
    }

    public void GainMeter(int val)
    {
        meter += val;
        AdjustSuper();
    }

    public void PowerUp(int val)
    {
        foreach(Transform child in transform)
        {
            if(child.gameObject.GetComponent<AttackData>() != null)
            {
                child.gameObject.GetComponent<AttackData>().AdjustDamage(val);
            }
        }
    }
}
