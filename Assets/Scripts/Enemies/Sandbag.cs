﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sandbag : MonoBehaviour {

    private bool stunned;
    private int stunTimer;
    private int knockbackDist;
    private int knockdownTimer;
    private bool knockback;
    private bool inAir;

    private int damageWhileStunned;

    private bool facingRight;

    private Rigidbody2D rb2d;

    private float startY;

    public SpriteRenderer sprite;
    public int health;
    private bool dying;
    private int dyingTimer;
    private int blinkTimer;

    private bool isHit;
    
    public bool PlayerOwned;
    private string checkHit;

    // Use this for initialization
    void Start () {
        stunned = false;
        stunTimer = 0;
        knockbackDist = 2;
        knockdownTimer = 30;
        damageWhileStunned = 0;
        knockback = false;
        inAir = false;
        facingRight = true;

        rb2d = GetComponent<Rigidbody2D>();

        //health = 100;
        dying = false;
        dyingTimer = 30;
        blinkTimer = 5;

        isHit = false;

        if (PlayerOwned)
        {
            checkHit = "Attack";
        }
        else
        {
            checkHit = "PlayerAttack";
        }
    }

    // Update is called once per frame
    void Update () {
        if (facingRight)
        {
            //flip
            sprite.flipX = true;
            knockbackDist = -2;
        }
        else
        {
            //flip
            sprite.flipX = false;
            knockbackDist = 2;
        }

        if (stunned && stunTimer > 0 && !knockback)
        {
            //stay still, play hit animations
            stunTimer--;
        }
        else if (stunned && stunTimer==0 && !knockback)
        {
            stunned = false;
        }

        if(health <= 0 && !dying)
        {
            BoxCollider2D[] boxes = gameObject.GetComponents<BoxCollider2D>();
            boxes[0].enabled = false;
            boxes[1].enabled = false;
            dying = true;
        }

        if (dying && !knockback)
        {
            if(blinkTimer == 0)
            {
                sprite.enabled = !sprite.GetComponent<SpriteRenderer>().enabled;
                blinkTimer = 5;
            }

            if (dyingTimer == 0)
            {
                Destroy(this.gameObject);
            }

            dyingTimer--;
            blinkTimer--;
        }
	}

    // For physics stuff, in this case, knockback
    void FixedUpdate()
    {
        if (knockback && !inAir)
        {
            inAir = true;
            rb2d.gravityScale = 1;
            startY = transform.position.y;
            BoxCollider2D[] boxes = gameObject.GetComponents<BoxCollider2D>();
            boxes[1].enabled = false;

            if (facingRight)
                rb2d.AddForce(new Vector2(-300,200));
            else
                rb2d.AddForce(new Vector2(300, 200));
        }
        else if (inAir && transform.position.y <= startY)
        {
            rb2d.gravityScale = 0;
            rb2d.velocity = new Vector2(0.0f,0.0f);
            inAir = false;
            knockback = false;
            BoxCollider2D[] boxes = gameObject.GetComponents<BoxCollider2D>();
            boxes[1].enabled = true;
        }
    }

    void LateUpdate()
    {
        sprite.sortingOrder = -(int)(transform.position.y * 100);
    }

    public void GetHit(int damage, int stunTime, bool launch, int direction)
    {
        if (!isHit)
        {
            stunTimer = stunTime;
            stunned = true;
            knockback = launch;
            health -= damage;
            damageWhileStunned += damage;
            if (health <= 0)
            {
                health = 0;
                knockback = true;
            }

            print(health);

            if (direction < 0)
                facingRight = false;
            else
                facingRight = true;

            isHit = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == checkHit)
        {
            isHit = false;
        }
    }
}
