﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
This enemy walks around to 3 points, aligns with a player, then charges and attack.
*/

public class ChargerAction : MonoBehaviour {

    private bool sawPlayer;
    private bool attackPlayer;
    private bool moving;

    public float MaxSpeed;
    public float speed;             //Floating point variable to store the player's movement speed.
    //private AttackActions attack;

    private float Left, Right, Bottom;
    private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.

    private bool facingRight;

    private Vector3 goalPoint;

    private GameObject player;
    
    private int waitToMove;
    private int waitToAttack;

    private float knownPlayerY;

    // Use this for initialization
    void Start () {
        sawPlayer = false;
        attackPlayer = false;
        moving = true;

        waitToAttack = 0;
        waitToMove = 60;
        //Get and store a reference to the Rigidbody2D component so that we can access it.

        MaxSpeed = 8.0f;
        speed = 2.0f;

        //attack = gameObject.GetComponent<AttackActions>();

        Left = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x + 0.5f;
        Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;
        Bottom = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y + 1.0f;
        rb2d = GetComponent<Rigidbody2D>();


        float x = transform.position.x + Random.Range(-2, 2);
        float y = transform.position.y + Random.Range(-2, 2);
        goalPoint = new Vector3(x, y);

        if (x < Left)
        {
            x = Left;
        }
        else if (x > Right)
        {
            x = Right;
        }

        if (y < Bottom)
        {
            y = Bottom;
        }
        else if (y > 0.0f)
        {
            y = 0.0f;
        }

        if (goalPoint.x > transform.position.x)
        {
            facingRight = true;
            /*if (attack.GetSide() < 0)
                attack.FlipAttacks();*/
        }
        else
        {
            facingRight = false;
            /*if (attack.GetSide() > 0)
                attack.FlipAttacks();*/
        }

        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        /*if (facingRight && player.transform.position.x - transform.position.x <= 2)
        {
            sawPlayer = true;
        }
        else if (!facingRight && player.transform.position.x - transform.position.x >= -2)
        {
            sawPlayer = true;
        }*/

        if (!sawPlayer && !moving && waitToAttack < 3)
        {
            //pick to location to mill about to
            float x = transform.position.x + Random.Range(-2, 2);
            float y = transform.position.y + Random.Range(-2, 2);

            if (x < Left)
            {
                x = Left;
            }
            else if (x > Right)
            {
                x = Right;
            }

            if (y < Bottom)
            {
                y = Bottom;
            }
            else if (y > 0.0f)
            {
                y = 0.0f;
            }

            goalPoint = new Vector3(x, y);
            if (goalPoint.x > transform.position.x)
            {
                facingRight = true;
                /*if (attack.GetSide() < 0)
                    attack.FlipAttacks();*/
            }
            else
            {
                facingRight = false;
                /*if (attack.GetSide() > 0)
                    attack.FlipAttacks();*/
            }
            moving = true;
        }

        if (waitToAttack >= 3 && !moving)
        {
            //move to player's y coordinate
            knownPlayerY = player.transform.position.y;
            goalPoint.y = player.transform.position.y;
            goalPoint.x = transform.position.x + Random.Range(-2, 2);

            if (goalPoint.x > transform.position.x)
            {
                facingRight = true;
                /*if (attack.GetSide() < 0)
                    attack.FlipAttacks();*/
            }
            else
            {
                facingRight = false;
                /*if (attack.GetSide() > 0)
                    attack.FlipAttacks();*/
            }
            moving = true;
        }
        else if(attackPlayer && waitToAttack == 3 && !moving)
        {
            if (facingRight)
                goalPoint = new Vector3(this.transform.position.x + 2, this.transform.position.y, this.transform.position.z);
            else if (!facingRight)
                goalPoint = new Vector3(this.transform.position.x + 2, this.transform.position.y, this.transform.position.z);

            moving = true;
        }
        else if (sawPlayer && attackPlayer)
        {
            //attack player
            /*attack.LAttack();
            if (attack.GetLAttackCount() == 3)
            {
                attackPlayer = false;
            }*/
        }
        if(waitToMove >= 0)
        {
            waitToMove--;
        }
    }

    void FixedUpdate()
    {
        if (moving && waitToMove <= 0)
        {
            //mill about, move towards goal point
            Vector3 startPoint = this.transform.position;
            float step = speed * Time.deltaTime; 
            if (attackPlayer)
                step = MaxSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(startPoint, goalPoint, step);
        }

        if (this.transform.position == goalPoint && waitToMove <=0)
        {
            if (waitToAttack >= 0 && waitToAttack < 3)
                waitToAttack++;
            else if (waitToAttack == 3)
            { 
                attackPlayer = true;
                waitToAttack++;
            }
            else if(waitToAttack > 3)
            {
                waitToAttack = 0;
                attackPlayer = false;
            }
            moving = false;
            waitToMove = 60;
            print(waitToAttack);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            goalPoint = transform.position;
            moving = false;
            attackPlayer = true;
        }
    }
}
