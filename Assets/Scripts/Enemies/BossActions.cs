﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossActions : MonoBehaviour {

    public int type; // 0 = L, 1 = H, 2 = fast

    private bool sawPlayer;
    private bool attackPlayer;
    private bool moving;

    private bool canSuper;

    private int attackTimer;

    public float MaxSpeed;
    public float speed;             //Floating point variable to store the player's movement speed.
    private AttackActions attack;

    private float Left, Right, Bottom;
    private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.

    private bool facingRight;

    private Vector3 goalPoint;

    private GameObject[] player;
    private int playerSeen;

    private int waitTimer;
    private bool wait;

    private HitHandler hitHandler;

    private int pointCount;

    public Animator spriteAnimator;
    public SpriteRenderer sprite;

    // Use this for initialization
    void Start()
    {
        sawPlayer = false;
        attackPlayer = false;
        moving = true;

        canSuper = true;

        //Get and store a reference to the Rigidbody2D component so that we can access it.

        type = Random.Range(0, 1);

        MaxSpeed = 2.0f;
        speed = 2.0f;

        attack = gameObject.GetComponent<AttackActions>();

        Left = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x + 0.5f;
        Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;
        Bottom = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y + 1.0f;
        rb2d = GetComponent<Rigidbody2D>();


        float x = transform.position.x + Random.Range(-1.0f, 1.0f);
        float y = transform.position.y + Random.Range(-1.0f, 1.0f);
        goalPoint = new Vector3(x, y);

        if (x < Left)
        {
            x = Left;
        }
        else if (x > Right)
        {
            x = Right;
        }

        if (y < Bottom)
        {
            y = Bottom;
        }
        else if (y > 0.0f)
        {
            y = 0.0f;
        }

        if (goalPoint.x > transform.position.x)
        {
            if (sprite.transform.localPosition.x <= 0)
            {
                sprite.transform.localPosition = new Vector3(sprite.transform.localPosition.x * -1.0f,
                    sprite.transform.localPosition.y, sprite.transform.localPosition.z);
            }
            sprite.flipX = false;

            facingRight = true;

            if (attack.GetSide() < 0)
                attack.FlipAttacks();
        }
        else
        {
            if (sprite.transform.localPosition.x >= 0)
            {
                sprite.transform.localPosition = new Vector3(sprite.transform.localPosition.x * -1.0f,
                    sprite.transform.localPosition.y, sprite.transform.localPosition.z);
            }
            sprite.flipX = true;

            facingRight = false;
            if (attack.GetSide() > 0)
                attack.FlipAttacks();
        }

        player = GameObject.FindGameObjectsWithTag("Player");
        //waitTimer = 60;
        wait = false;
        attackTimer = 15;

        hitHandler = gameObject.GetComponent<HitHandler>();
        pointCount = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (!hitHandler.IsStunned())
        {

            if (!sawPlayer && pointCount >= 1)
            {
                int playerToSee = Random.Range(0, player.Length);
                GameObject playerCheck = player[playerToSee];

                if ((facingRight && playerCheck.transform.position.x - transform.position.x <= 0.5f)
                    || (!facingRight && playerCheck.transform.position.x - transform.position.x >= -0.5f)
                    && !sawPlayer)
                {
                    sawPlayer = true;
                    playerSeen = playerToSee;
                }
                pointCount = 0;
            }

            if (!sawPlayer && !moving && !wait)
            {
                //pick to location to mill about to
                float x = transform.position.x + Random.Range(-1.0f, 1.0f);
                float y = transform.position.y + Random.Range(-1.0f, 1.0f);

                if (x < Left)
                {
                    x = Left;
                }
                else if (x > Right)
                {
                    x = Right;
                }

                if (y < Bottom)
                {
                    y = Bottom;
                }
                else if (y > 0.0f)
                {
                    y = 0.0f;
                }

                goalPoint = new Vector3(x, y);
                if (goalPoint.x > transform.position.x)
                {
                    if (sprite.transform.localPosition.x <= 0)
                    {
                        sprite.transform.localPosition = new Vector3(sprite.transform.localPosition.x * -1.0f,
                            sprite.transform.localPosition.y, sprite.transform.localPosition.z);
                    }
                    sprite.flipX = false;

                    facingRight = true;
                    if (attack.GetSide() < 0)
                        attack.FlipAttacks();
                }
                else
                {
                    if (sprite.transform.localPosition.x >= 0)
                    {
                        sprite.transform.localPosition = new Vector3(sprite.transform.localPosition.x * -1.0f,
                            sprite.transform.localPosition.y, sprite.transform.localPosition.z);
                    }
                    sprite.flipX = true;

                    facingRight = false;
                    if (attack.GetSide() > 0)
                        attack.FlipAttacks();
                }
                moving = true;
            }

            if (sawPlayer && !attackPlayer && !wait)
            {
                //move to player
                goalPoint = player[playerSeen].transform.position;
                if (goalPoint.x > transform.position.x)
                {
                    if (sprite.transform.localPosition.x <= 0)
                    {
                        sprite.transform.localPosition = new Vector3(sprite.transform.localPosition.x * -1.0f,
                            sprite.transform.localPosition.y, sprite.transform.localPosition.z);
                    }
                    sprite.flipX = false;

                    facingRight = true;
                    if (attack.GetSide() < 0)
                        attack.FlipAttacks();
                }
                else
                {
                    if (sprite.transform.localPosition.x >= 0)
                    {
                        sprite.transform.localPosition = new Vector3(sprite.transform.localPosition.x * -1.0f,
                            sprite.transform.localPosition.y, sprite.transform.localPosition.z);
                    }
                    sprite.flipX = true;

                    facingRight = false;
                    if (attack.GetSide() > 0)
                        attack.FlipAttacks();
                }
                moving = true;
            }
            else if (sawPlayer && attackPlayer && !wait)
            {
                //attack player
                if (type == 0)
                {
                    if (attackTimer <= 0 && attack.GetLAttackCount() < 3)
                    {
                        attack.LAttack();
                        attackTimer = 15;
                    }
                    else if (attackTimer >= 0)
                    {
                        attackTimer--;
                    }
                    if (attack.GetLAttackCount() == 3 && attackTimer <= 0)
                    {
                        attackPlayer = false;
                        wait = true;
                        type = Random.Range(0,2);
                        if (!canSuper)
                            canSuper = true;
                    }
                }

                // Attack player with a heavy attack
                else if (type == 1)
                {

                    if (attackTimer <= 0)
                    {
                        attack.HAttack();
                        attackTimer = 30;
                    }
                    else if (attackTimer >= 0)
                    {
                        attackTimer--;
                    }
                    if (attackTimer <= 0)
                    {
                        attackPlayer = false;
                        wait = true;
                        type = Random.Range(0,2);
                        if (!canSuper)
                            canSuper = true;
                    }
                    //spriteAnimator.Play("Heavy", -1);
                    /*attack.HAttack();
                    attackPlayer = false;
                    wait = true;
                    if(!canSuper)
                        canSuper = true;*/
                }
                // Attack player with super attack
                else if(type == 2)
                {
                    //spriteAnimator.Play("Super", -1);
                    attack.SAttack();

                    attackPlayer = false;
                    wait = true;
                    canSuper = false;
                    waitTimer = 100;
                }
            }
            if (/*sawPlayer && !attackPlayer && */ wait)
            {
                if (waitTimer != 0)
                    waitTimer--;
                else
                {
                    waitTimer = 60;
                    wait = false;
                    sawPlayer = false;
                    type = Random.Range(0, 2);
                }
            }
        }

        if (hitHandler.IsStunned())
        {
            if (transform.position.x <= Left || transform.position.x >= Right)
            {
                rb2d.velocity = new Vector2(0.0f, rb2d.velocity.y);
            }

            if (transform.position.y <= Bottom)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0.0f);
            }
        }
    }

    void FixedUpdate()
    {
        if (!hitHandler.IsStunned())
        {
            if (moving)
            {
                spriteAnimator.Play("Walk", -1);
                //mill about, move towards goal point
                Vector3 startPoint = this.transform.position;
                float step = speed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(startPoint, goalPoint, step);
            }

            if (this.transform.position == goalPoint)
            {
                moving = false;
                pointCount++;
            }
        }
        if (hitHandler.IsStunned())
        {
            if (!hitHandler.isDown())
                spriteAnimator.Play("Hit", -1);
            else if (hitHandler.isDown())
                spriteAnimator.Play("Down", -1);

            if(hitHandler.GetHealth() <= hitHandler.GetMaxHealth() / 2 && type != 2)
            {
                type = 2;
            }
        }
        if (!moving && !hitHandler.IsStunned() && !attackPlayer && attackTimer <= 0 && waitTimer <= 0)
        {
            spriteAnimator.Play("Idle", -1);
        }
    }

    void LateUpdate()
    {
        sprite.sortingOrder = -(int)(transform.position.y * 100);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            goalPoint = transform.position;
            moving = false;
            attackPlayer = true;
        }
        if (collision.gameObject.tag == "Enemy")
        {
            goalPoint = transform.position;
            moving = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerRange")
        {
            goalPoint = transform.position;
            moving = false;
            attackPlayer = true;
        }
    }
}
