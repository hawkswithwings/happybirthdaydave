﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackData : MonoBehaviour {

    public bool usesMeter;

    public int damage;
    public int stun;
    public bool launch;
    private AudioSource sound;

    private string hitCheck;

	// Use this for initialization
	void Start () {
        if (gameObject.tag == "PlayerAttack" || gameObject.tag == "Assist")
        {
            hitCheck = "Enemy";
        }
        else
        {
            hitCheck = "Player";
        }

        sound = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Obstacle") && !usesMeter)
        {
            if(other.gameObject.GetComponent<BreakableItem>() != null)
            {
                if (sound != null)
                    sound.Play();
                other.gameObject.GetComponent<BreakableItem>().DealDamage(damage);
            }
        }

        if (other.gameObject.tag == hitCheck)
        {
            //print(hitCheck);
            int direction = 1;

            //if (transform.position.x < other.gameObject.transform.position.x)
            float start = gameObject.GetComponent<BoxCollider2D>().bounds.min.x;
            if(transform.localPosition.x < 0)
            {
                start = gameObject.GetComponent<BoxCollider2D>().bounds.max.x;
            }


            if (start < other.gameObject.transform.position.x)
                direction = -1;

            if (other.gameObject.GetComponent<HitHandler>() != null)
            {
                if(sound != null)
                    sound.Play();
                other.gameObject.GetComponent<HitHandler>().GetHit(damage, stun, launch, direction);

                if(gameObject.tag== "PlayerAttack" && !usesMeter)
                {
                    transform.parent.gameObject.GetComponent<PlayerController>().GainMeter(5);
                }

            }
            else if (other.gameObject.GetComponent<Sandbag>() != null)
            {
                other.gameObject.GetComponent<Sandbag>().GetHit(damage, stun, launch, direction);
            }
        }
    }

    public void AdjustDamage(int val)
    {
        damage += val;
    }
    
}
