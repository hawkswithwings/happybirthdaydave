﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitHandler : MonoBehaviour
{

    private bool stunned;
    private int stunTimer;
    private int knockbackDist;
    private int knockdownTimer;
    private bool knockback;
    private bool inAir;

    private int maxHealth;

    private int damageWhileStunned;

    private bool facingRight;

    private Rigidbody2D rb2d;

    private float startY;

    public SpriteRenderer sprite;
    public int health;
    private bool dying;
    private int dyingTimer;
    private int blinkTimer;

    private bool isHit;

    public bool PlayerOwned;
    private string checkHit;

    public GameObject healthItem;

    public Transform startingPos;

    private AttackActions attackHandler;

    // Use this for initialization
    void Start()
    {
        stunned = false;
        stunTimer = 0;
        knockbackDist = 2;
        knockdownTimer = 0;
        damageWhileStunned = 0;
        knockback = false;
        inAir = false;
        facingRight = true;

        rb2d = GetComponent<Rigidbody2D>();

        //health = 100;
        maxHealth = health;
        dying = false;
        dyingTimer = 30;
        blinkTimer = 5;

        isHit = false;

        if (PlayerOwned)
        {
            checkHit = "Attack";
        }
        else
        {
            checkHit = "PlayerAttack";
        }

        attackHandler = gameObject.GetComponent<AttackActions>();
    }

    // Update is called once per frame
    void Update()
    {
        if (facingRight)
        {
            //flip knockbackDist
            //sprite.flipX = true;
            knockbackDist = -2;
        }
        else
        {
            //flip knockbackDist
            //sprite.flipX = false;
            knockbackDist = 2;
        }

        if (!dying)
        {
            if (stunned && stunTimer > 0 && !knockback)
            {
                //stay still, play hit animations
                if (knockdownTimer > 0)
                {
                    knockdownTimer--;
                    if(knockdownTimer == 0 && health > 0)
                    {
                        BoxCollider2D[] boxes = gameObject.GetComponents<BoxCollider2D>();
                        boxes[0].enabled = true;
                        boxes[1].enabled = true;
                    }
                }
                stunTimer--;
            }
            else if (stunned && stunTimer == 0 && !knockback && health > 0)
            {
                stunned = false;
            }

            if (health <= 0 && !dying)
            {
                BoxCollider2D[] boxes = gameObject.GetComponents<BoxCollider2D>();
                boxes[0].enabled = false;
                boxes[1].enabled = false;
                dying = true;
                stunned = true;
            }
        }

        if (dying && !knockback)
        {
            if (blinkTimer == 0)
            {
                if (sprite != null)
                {
                    sprite.enabled = !sprite.GetComponent<SpriteRenderer>().enabled;
                }
                blinkTimer = 5;
            }

            if (dyingTimer == 0)
            {
                if(gameObject.tag == "Player")
                {
                    health = 100;
                    gameObject.GetComponent<PlayerController>().AdjustHealth();
                    dying = false;
                    sprite.enabled = true;
                    knockdownTimer = 1;
                    stunTimer = 1;
                    dyingTimer = 31;
                    gameObject.transform.position = startingPos.position;
                }
                else
                {
                    int drop = Random.Range(1, 101);
                    if(drop <= 25 && gameObject.name != "Boss_Dan" && gameObject.name != "Boss_Bill")
                    {
                        GameObject healthDrop = Instantiate(healthItem) as GameObject;
                        healthDrop.transform.position = transform.position;
                        healthDrop.GetComponent<SpriteRenderer>().sortingOrder = -(int)(healthDrop.transform.position.y * 100);
                    }
                    Destroy(this.gameObject);
                }
            }

            dyingTimer--;
            blinkTimer--;
        }
    }

    // For physics stuff, in this case, knockback
    void FixedUpdate()
    {
        if (knockback && !inAir)
        {
            inAir = true;
            rb2d.gravityScale = 1;
            startY = transform.position.y;
            BoxCollider2D[] boxes = gameObject.GetComponents<BoxCollider2D>();
            boxes[0].enabled = false;
            boxes[1].enabled = false;

            if (facingRight)
                rb2d.AddForce(new Vector2(-200, 200));
            else
                rb2d.AddForce(new Vector2(200, 200));
        }
        else if (inAir && transform.position.y <= startY)
        {
            rb2d.gravityScale = 0;
            rb2d.velocity = new Vector2(0.0f, 0.0f);
            inAir = false;
            knockback = false;
            //BoxCollider2D[] boxes = gameObject.GetComponents<BoxCollider2D>();
            //boxes[1].enabled = true;
            knockdownTimer = 60;
            stunTimer = 60;
        }
    }

    void LateUpdate()
    {
        //sprite.sortingOrder = -(int)(transform.position.y * 100);
    }

    public void GetHit(int damage, int stunTime, bool launch, int direction)
    {
        if (!isHit)
        {
            stunTimer = stunTime;
            stunned = true;
            knockback = launch;
            health -= damage;
            damageWhileStunned += damage;
            if (health <= 0)
            {
                health = 0;
                knockback = true;
            }

            if (direction < 0)
                facingRight = false;
            else
                facingRight = true;

            if (PlayerOwned)
            {
                gameObject.GetComponent<PlayerController>().AdjustHealth();
                gameObject.GetComponent<PlayerController>().GainMeter(10);
                FlipPlayerStuff();
            }
            else
            {
                FlipEnemyStuff();
            }

            isHit = true;
        }
    }

    public void AssistStun(int stunTime)
    {
        stunTimer = stunTime;
        stunned = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == checkHit)
        {
            isHit = false;
        }
    }

    public bool IsStunned()
    {
        if(stunned || dying || knockback)
        {
            return true;
        }
        return false;
    }

    public bool isDown()
    {
        if (knockdownTimer > 0)
        {
            return true;
        }
        else
            return false;
    }

    public void FlipPlayerStuff()
    {
        PlayerController player = gameObject.GetComponent<PlayerController>();
        if((facingRight && !player.IsFacingRight()) || (!facingRight && player.IsFacingRight()))
        {
            player.FlipPlayer();
        }
    }

    public void FlipEnemyStuff()
    {

    }

    public int GetHealth()
    {
        return health;
    }

    public void AddHealth(int val)
    {
        health += val;
        if(health > 100)
        {
            health = 100;
        }
    }

    public int GetMaxHealth()
    {
        return maxHealth;
    }
}