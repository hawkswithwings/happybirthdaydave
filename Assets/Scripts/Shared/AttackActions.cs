﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackActions : MonoBehaviour {

    public GameObject range;

    public GameObject idle;
    public GameObject LA1;
    public GameObject LA2;
    public GameObject LA3;
    public GameObject HA;
    public GameObject SpA;
    public GameObject SA;

    private bool LA1Active;
    private bool LA2Active;
    private bool LA3Active;
    private bool HAActive;
    private bool SpAActive;
    private bool SAActive;

    private int LA1Timer;
    private int LA2Timer;
    private int LA3Timer;
    private int HATimer;
    private int SpATimer;
    private int SATimer;

    private int maxSATime;

    private int LACount;

    private bool LACDStart;
    private bool HACDStart;
    private bool SpACDStart;
    private bool SACDStart;
    private int LACooldown;
    private int HACooldown;
    private int SpACooldown;
    private int SACooldown;

    public Animator spriteAnimator;

    private bool isHit;

    // Use this for initialization
    void Start () {
        LA1Active = false;
        LA2Active = false;
        LA3Active = false;
        HAActive = false;
        SpAActive = false;
        SAActive = false;

        LA1Timer = 4;
        LA2Timer = 6;
        LA3Timer = 8;
        HATimer = 15;
        SpATimer = 30;
        SATimer = 41;

        maxSATime = 41;

        if (gameObject.name == "Boss_Dan" || gameObject.name == "Boss_Bill")
            maxSATime = 51;

        //LA1Damage = 5;
        //LA2Damage = 5;
        //LA3Damage = 10;
        //HADamage = 15;

        LACount = 0;

        LACDStart = false;
        HACDStart = false;
        SpACDStart = false;
        SACDStart = false;

        LACooldown = 15;
        HACooldown = 30;
        SpACooldown = 15;
        SACooldown = 40;

        isHit = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!LACDStart)
        {
            if (LA1Active && !LA1.activeSelf)
            {
                //idle.SetActive(false);
                LA1.SetActive(true);
                LA1.GetComponent<BoxCollider2D>().enabled = true;
            }
            else if (LA1Active && LA1Timer > 0)
            {
                LA1Timer--;
            }
            else if (LA1Active && LA1Timer == 0)
            {
                LA1Active = false;
                LA1Timer = 4;
                //LA1.SetActive(false);
                LA1.GetComponent<BoxCollider2D>().enabled = false;
                LACDStart = true;
            }

            if (LA2Active && !LA2.activeSelf && !LA1Active)
            {
                LA1.SetActive(false);
                LA1.GetComponent<BoxCollider2D>().enabled = true;
                LA2.SetActive(true);
                LA2.GetComponent<BoxCollider2D>().enabled = true;
            }
            else if (LA2Active && LA2Timer > 0)
            {
                LA2Timer--;
            }
            else if (LA2Active && LA2Timer == 0)
            {
                LA2Active = false;
                LA2Timer = 6;
                //LA2.SetActive(false);
                LA2.GetComponent<BoxCollider2D>().enabled = false;
                LACDStart = true;
            }

            if (LA3Active && !LA3.activeSelf && !LA2Active)
            {
                LA2.SetActive(false);
                LA2.GetComponent<BoxCollider2D>().enabled = true;
                LA3.SetActive(true);
                LA3.GetComponent<BoxCollider2D>().enabled = true;
            }
            else if (LA3Active && LA3Timer > 0)
            {
                LA3Timer--;
            }
            else if (LA3Active && LA3Timer == 0)
            {
                LA3Active = false;
                LA3Timer = 8;
                //LA3.SetActive(false);
                LA3.GetComponent<BoxCollider2D>().enabled = false;
                LACDStart = true;
            }
        }
        else if (LACDStart)
        {
            LACooldown--;
            if (LACooldown == 0)
            {
                LACDStart = false;
                LACooldown = 15;
                LACount = 0;
                LA1.SetActive(false);
                LA2.SetActive(false);
                LA3.SetActive(false);
                LA1.GetComponent<BoxCollider2D>().enabled = true;
                LA2.GetComponent<BoxCollider2D>().enabled = true;
                LA3.GetComponent<BoxCollider2D>().enabled = true;
                //idle.SetActive(true);
            }
        }


        if (!HACDStart)
        {
            if (HAActive && !HA.activeSelf)
            {
                //idle.SetActive(false);
                LA1.SetActive(false);
                LA2.SetActive(false);
                LA3.SetActive(false);
                LACDStart = false;
                LACooldown = 15;

                HA.SetActive(true);
                HA.GetComponent<BoxCollider2D>().enabled = true;
            }
            else if (HAActive && HATimer > 0)
            {
                HATimer--;
            }
            else if (HAActive && HATimer == 0)
            {
                HAActive = false;
                HATimer = 15;
                //HA.SetActive(false);
                HACDStart = true;
                HA.GetComponent<BoxCollider2D>().enabled = false;
            }
        }
        else if (HACDStart)
        {
            HACooldown--;
            if (HACooldown == 0)
            {
                HACDStart = false;
                HACooldown = 30;
                HA.SetActive(false);
                HA.GetComponent<BoxCollider2D>().enabled = true;
                //idle.SetActive(true);
            }
        }

        if (!SpACDStart)
        {
            if (SpAActive && !SpA.activeSelf)
            {
                //idle.SetActive(false);
                LA1.SetActive(false);
                LA2.SetActive(false);
                LA3.SetActive(false);
                HA.SetActive(false);
                LACDStart = false;
                LACooldown = 15;
                HACDStart = false;
                HACooldown = 30;

                SpA.SetActive(true);
                SpA.GetComponent<BoxCollider2D>().enabled = true;

                if (SpA.GetComponent<DaveSpecialAttack>() != null)
                {
                    SpA.GetComponent<DaveSpecialAttack>().PickPhrase();
                }
            }
            else if (SpAActive && SpATimer > 0)
            {
                SpATimer--;
            }
            else if (SpAActive && SpATimer == 0)
            {
                SpAActive = false;
                SpATimer = 30;
                //HA.SetActive(false);
                SpACDStart = true;
                SpA.GetComponent<BoxCollider2D>().enabled = false;
            }
        }
        else if (SpACDStart)
        {
            SpACooldown--;
            if (SpACooldown == 0)
            {
                SpACDStart = false;
                SpACooldown = 30;
                SpA.SetActive(false);
                SpA.GetComponent<BoxCollider2D>().enabled = true;
                //idle.SetActive(true);
            }
        }

        if (!SACDStart)
        {
            if (SAActive && SATimer == maxSATime/* && !SA.activeSelf*/)
            {
                //idle.SetActive(false);
                LA1.SetActive(false);
                LA2.SetActive(false);
                LA3.SetActive(false);
                HA.SetActive(false);
                LACDStart = false;
                LACooldown = 15;
                HACDStart = false;
                HACooldown = 30;

                SATimer--;
                //SA.GetComponent<BoxCollider2D>().enabled = false;
                //SA.SetActive(true);
            }
            else if (SAActive && SATimer > 0)
            {
                SATimer--;
            }
            else if (SAActive && SATimer == 0)
            {
                SAActive = false;
                SATimer = maxSATime;
                //HA.SetActive(false);
                SACDStart = true;
                if (gameObject.name == "Boss_Dan" || gameObject.name == "Boss_Bill")
                {
                    //SATimer = 50;
                    gameObject.GetComponent<AudioSource>().Stop();
                    SA.GetComponent<SpriteRenderer>().enabled = true;
                }
                //SA.GetComponent<BoxCollider2D>().enabled = true;
                SA.SetActive(true);
            }
        }
        else if (SACDStart)
        {
            SACooldown--;
            if (SACooldown == 0)
            {
                SACDStart = false;
                SACooldown = 40;
                SA.SetActive(false);
                //SA.GetComponent<BoxCollider2D>().enabled = false;
                //if (gameObject.name == "Boss_Dan" || gameObject.name == "Boss_Bill")
                    //SA.GetComponent<SpriteRenderer>().enabled = false;
                gameObject.GetComponents<BoxCollider2D>()[0].enabled = true;
                gameObject.GetComponents<BoxCollider2D>()[1].enabled = true;
                //idle.SetActive(true);
            }
        }
    }

    void LateUpdate()
    {
        //LA1.GetComponent<SpriteRenderer>().sortingOrder = -(int)(transform.position.y * 100);
        //LA2.GetComponent<SpriteRenderer>().sortingOrder = -(int)(transform.position.y * 100);
        //LA3.GetComponent<SpriteRenderer>().sortingOrder = -(int)(transform.position.y * 100);
        //HA.GetComponent<SpriteRenderer>().sortingOrder = -(int)(transform.position.y * 100);
        if(SpA != null)
            SpA.GetComponent<SpriteRenderer>().sortingOrder = -(int)(transform.position.y * 100);
    }

    public bool LAttack()
    {
        if (LACount == 0 && !LA1Active && !LA2Active && !LA3Active && !HAActive && !LACDStart && 
            !HACDStart && !SpAActive && !SAActive && !SpACDStart)
        {
            if(spriteAnimator != null)
                spriteAnimator.Play("Punch1", -1);
            LACount++;
            LA1Active = true;
            return true;
        }
        else if (LACount == 1 && !LA1Active && !LA2Active && !LA3Active && !HAActive && LACDStart && 
            !HACDStart && !SpAActive && !SAActive && !SpACDStart)
        {
            if (spriteAnimator != null)
                spriteAnimator.Play("Punch2", -1);
            LACount++;
            LACDStart = false;
            LACooldown = 15;
            LA2Active = true;
            return true;
        }
        else if (LACount == 2 && !LA1Active && !LA2Active && !LA3Active && !HAActive && LACDStart && 
            !HACDStart && !SpAActive && !SAActive && !SpACDStart)
        {
            if (spriteAnimator != null)
                spriteAnimator.Play("Punch3", -1);
            LACount++;
            LACDStart = false;

            if (gameObject.name == "Boss_Dan" || gameObject.name == "Boss_Bill")
                LACooldown = 15;
            else
                LACooldown = 30;
            LA3Active = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool HAttack()
    {
        if (!LA1Active && !LA2Active && !LA3Active && !HAActive && !HACDStart && !SpAActive && !SAActive
            && !SpACDStart)
        {
            if (spriteAnimator != null)
                spriteAnimator.Play("Heavy", -1);
            LACount = 0;
            LACDStart = false;
            LACooldown = 15;

            HAActive = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SpAttack()
    {
        if (!LA1Active && !LA2Active && !LA3Active && !HAActive && !SpAActive && !SAActive && !SpACDStart && !SACDStart)
        {
            if (spriteAnimator != null)
                spriteAnimator.Play("Special", -1);
            LACount = 0;
            LACDStart = false;
            LACooldown = 15;

            HACDStart = false;
            HACooldown = 30;

            SpAActive = true;
            return true;
        }
        return false;
    }

    public bool SAttack()
    {
        if (!LA1Active && !LA2Active && !LA3Active && !HAActive && !SpAActive && !SAActive && !SpACDStart && !SACDStart)
        {
            if (spriteAnimator != null)
                spriteAnimator.Play("Super", -1);

            if(gameObject.name == "Boss_Dan" || gameObject.name == "Boss_Bill")
            {
                gameObject.GetComponent<AudioSource>().Play();
            }

            LACount = 0;
            LACDStart = false;
            LACooldown = 15;

            HACDStart = false;
            HACooldown = 30;

            SAActive = true;

            gameObject.GetComponents<BoxCollider2D>()[0].enabled = false;
            gameObject.GetComponents<BoxCollider2D>()[1].enabled = false;

            return true;
        }
        return false;
    }

    public bool IsAttacking()
    {
        if (LA1Active || LA2Active || LA3Active || HAActive || SpAActive || SAActive || 
            LACDStart || HACDStart || SpACDStart || SACDStart )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void FlipAttacks()
    {
        //LA1.GetComponent<SpriteRenderer>().flipX = !LA1.GetComponent<SpriteRenderer>().flipX;
        //LA2.GetComponent<SpriteRenderer>().flipX = !LA2.GetComponent<SpriteRenderer>().flipX;
        //LA3.GetComponent<SpriteRenderer>().flipX = !LA3.GetComponent<SpriteRenderer>().flipX;
        //HA.GetComponent<SpriteRenderer>().flipX = !HA.GetComponent<SpriteRenderer>().flipX;
        

        LA1.GetComponent<BoxCollider2D>().offset = new Vector2(LA1.GetComponent<BoxCollider2D>().offset.x * -1.0f, 
            LA1.GetComponent<BoxCollider2D>().offset.y);
        LA2.GetComponent<BoxCollider2D>().offset = new Vector2(LA2.GetComponent<BoxCollider2D>().offset.x * -1.0f,
            LA2.GetComponent<BoxCollider2D>().offset.y);
        LA3.GetComponent<BoxCollider2D>().offset = new Vector2(LA3.GetComponent<BoxCollider2D>().offset.x * -1.0f,
            LA3.GetComponent<BoxCollider2D>().offset.y);
        HA.GetComponent<BoxCollider2D>().offset = new Vector2(HA.GetComponent<BoxCollider2D>().offset.x * -1.0f,
            HA.GetComponent<BoxCollider2D>().offset.y);
        

        LA1.transform.localPosition = new Vector3(LA1.transform.localPosition.x * -1.0f, LA1.transform.localPosition.y, 
            LA1.transform.localPosition.z);
        LA2.transform.localPosition = new Vector3(LA2.transform.localPosition.x * -1.0f, LA2.transform.localPosition.y, 
            LA2.transform.localPosition.z);
        LA3.transform.localPosition = new Vector3(LA3.transform.localPosition.x * -1.0f, LA3.transform.localPosition.y, 
            LA3.transform.localPosition.z);
        HA.transform.localPosition = new Vector3(HA.transform.localPosition.x * -1.0f, HA.transform.localPosition.y, 
            HA.transform.localPosition.z);
        

        if(SpA != null)
        {
            SpA.transform.localPosition = new Vector3(SpA.transform.localPosition.x * -1.0f, SpA.transform.localPosition.y,
                SpA.transform.localPosition.z);

            SpA.GetComponent<BoxCollider2D>().offset = new Vector2(SpA.GetComponent<BoxCollider2D>().offset.x * -1.0f,
            SpA.GetComponent<BoxCollider2D>().offset.y);

            if (SpA.GetComponent<SpriteRenderer>() != null)
            {
                SpA.GetComponent<SpriteRenderer>().flipX = !SpA.GetComponent<SpriteRenderer>().flipX;
                if (SpA.GetComponent<DaveSpecialAttack>() != null)
                {
                    SpA.GetComponent<DaveSpecialAttack>().AdjustWordsOffset();
                }
            }
        }

        if (SA != null)
        {
            SA.transform.localPosition = new Vector3(SA.transform.localPosition.x * -1.0f, SA.transform.localPosition.y,
                SA.transform.localPosition.z);

            SA.GetComponent<BoxCollider2D>().offset = new Vector2(SA.GetComponent<BoxCollider2D>().offset.x * -1.0f,
            SA.GetComponent<BoxCollider2D>().offset.y);

            if (SA.GetComponent<SpriteRenderer>() != null)
            {
                SA.GetComponent<SpriteRenderer>().flipX = !SA.GetComponent<SpriteRenderer>().flipX;
            }
        }
    }

    public int GetSide()
    {
        if (LA1.transform.localPosition.x > 0)
            return 1;
        else
            return -1;
    }

    public int GetLAttackCount()
    {
        return LACount;
    }

    public void IsHit(bool val)
    {
        isHit = val;
    }
}
