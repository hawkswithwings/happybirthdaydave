﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Assist : MonoBehaviour {

    public bool Nic, Tarkan, Will, John;

    public GameObject food;

    //private GameStateManager manager;
    public int timeToLive;

    public Animator spriteAnimator;

    public SpriteRenderer sprite;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (timeToLive == 0)
        {
            //manager.AssistDone();
            if (John)
                JohnAssistEnd();
            Destroy(gameObject);
        }
        else if(timeToLive == 60 && Nic){
            NicAssist();
            spriteAnimator.Play("Action", -1);
            food.SetActive(true);
            timeToLive--;
        }
        else if (timeToLive == 59 && Nic)
        {
            food.SetActive(false);
            timeToLive--;
        }
        else if(timeToLive == 270 && John)
        {
            spriteAnimator.Play("Action", -1);
            JohnAssist();
            timeToLive--;
        }
        else if(timeToLive == 20 && Will)
        {
            spriteAnimator.Play("Action", -1);
            WillAssist();
            timeToLive--;
        }
        else if(timeToLive == 101 && Tarkan)
        {
            spriteAnimator.Play("Action", -1);
            timeToLive--;
        }
        else if(timeToLive <= 100 && Tarkan)
        {
            TarkanAssist();
            timeToLive--;
        }

        else
        {
            timeToLive--;
        }
	}

    void LateUpdate()
    {
        sprite.sortingOrder = -(int)(transform.position.y * 100);
    }

    public void NicAssist()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].GetComponent<HitHandler>().AssistStun(180);
        }
    }

    public void WillAssist()
    {
        //Appears and makes 4 food items.
        GameObject food0 = Instantiate(food) as GameObject;
        food0.transform.position = new Vector3(this.transform.position.x + 1f, this.transform.position.y - 0.5f, this.transform.position.z);
        GameObject food1 = Instantiate(food) as GameObject;
        food1.transform.position = new Vector3(this.transform.position.x - 1f, this.transform.position.y - 0.5f, this.transform.position.z);
        GameObject food2 = Instantiate(food) as GameObject;
        food2.transform.position = new Vector3(this.transform.position.x + 1.2f, this.transform.position.y + 0.5f, this.transform.position.z);
        GameObject food3 = Instantiate(food) as GameObject;
        food3.transform.position = new Vector3(this.transform.position.x - 1.2f, this.transform.position.y + 0.5f, this.transform.position.z);
    }

    public void TarkanAssist()
    {
        //Charges forward and hits eveything.
        this.transform.position = new Vector3(this.transform.position.x + 0.1f, this.transform.position.y, this.transform.position.z);
    }

    public void JohnAssist()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach(GameObject player in players)
        {
            player.GetComponent<PlayerController>().PowerUp(10);
        }
    }

    public void JohnAssistEnd()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            player.GetComponent<PlayerController>().PowerUp(-10);
        }
    }
}
