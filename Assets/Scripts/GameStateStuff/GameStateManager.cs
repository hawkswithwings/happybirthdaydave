﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour {
    //This script should keep track of whether enemies are on screen so that the player can know if they
    //can continue or not.
    public CameraMovement cam;
    public PlayerController player;
    public PlayerController player2;

    public GameObject GoArrow;

    private bool enemiesChecked;
    private bool stopCamera;
    private GameObject[] enemiesOnScreen;

    private bool encounterActive;

    private AudioSource[] songs;
    private bool playBoss;

    private bool gameOverPlaying;
    private bool superPlaying;
    private int songPaused;

    private bool bossDefeated;

    private int superSongTimer;
    private float superTime;

    private bool assistActive;
    private GameObject activeAssist;
    public GameObject[] assists;
    public Transform assistSpawn;

    private int assistI;
    public GameObject assistOkay;
    private int assistCountdown;

    // Use this for initialization
    void Start() {
        enemiesChecked = false;
        stopCamera = false;
        encounterActive = false;

        songs = gameObject.GetComponents<AudioSource>();
        /* 0 = overworld
         * 1 = O Worm (randomly play 1 or 0 while walking)
         * 2 = Battle
         * 3 = Victory
         * 4 = Game Over
         * 5 = Super
         * 6 = Boss Theme
         */

        gameOverPlaying = false;
        superPlaying = false;
        songPaused = 0;

        bossDefeated = false;

        superSongTimer = 0;
        superTime = 0.0f;
        playBoss = false;

        assistActive = false;
        activeAssist = null;

        assistCountdown = 1200;
        assistI = 0;
    }

    // Update is called once per frame
    void Update() {
        if (!encounterActive)
        {
            if (cam.IsStopped() && !enemiesChecked && !cam.AtEnd())
            {
                enemiesOnScreen = GameObject.FindGameObjectsWithTag("Enemy");
                if (enemiesOnScreen.Length > 0)
                {
                    enemiesChecked = true;
                    cam.StopCamera(true);
                    player.SetScreenMoving(false);
                    player2.SetScreenMoving(false);
                }

                else if (enemiesOnScreen.Length == 0 && !songs[3].isPlaying)
                {
                    enemiesChecked = false;
                    cam.StopCamera(false);
                    player.SetScreenMoving(true);
                    player2.SetScreenMoving(true);
                }
            }
            else if (cam.IsStopped() && enemiesChecked && !cam.AtEnd())
            {
                stopCamera = false;
                for (int i = 0; i < enemiesOnScreen.Length; i++)
                {
                    if (enemiesOnScreen[i] != null)
                    {
                        stopCamera = true;
                    }
                }
                if (!stopCamera)
                {
                    enemiesChecked = false;
                    cam.StopCamera(false);
                    player.SetScreenMoving(true);
                    player2.SetScreenMoving(true);
                }
            }
            else if (cam.IsStopped() && cam.AtEnd())
            {
                player.SetScreenMoving(false);
                player2.SetScreenMoving(false);
            }

            if (!cam.IsStopped())
            {
                int i = Random.Range(0, 2);
                if (!songs[0].isPlaying && !songs[1].isPlaying && !songs[3].isPlaying && !songs[5].isPlaying)
                    songs[i].Play();
                if (Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Horizontal2") > 0 ||
                    Input.GetAxis("ControllerHorizontal") > 0 || Input.GetAxis("ControllerHorizontal2") > 0)
                    GoArrow.SetActive(false);
                else
                    GoArrow.SetActive(true);
            }
        }

        if(superPlaying && songs[5].isPlaying)
        {
            superSongTimer--;
            if(superSongTimer == 0)
            {
                superTime = songs[5].time;
                songs[5].Pause();
            }
        }

        if (superPlaying && !songs[5].isPlaying)
        {
            superPlaying = false;
            songs[songPaused].UnPause();
        }

        if (encounterActive)
        {
            if(gameOverPlaying && !songs[4].isPlaying)
            {
                gameOverPlaying = false;
                if(playBoss)
                    songs[6].Play();
                else
                    songs[2].Play();
            }
        }

        if(bossDefeated && !songs[3].isPlaying)
        {
            SceneManager.LoadScene("Credits");
        }

        if (assistActive)
        {
            assistCountdown--;
            if(assistCountdown == 0)
            {
                assistOkay.SetActive(true);
                assistCountdown = 1200;
                assistActive = false;
            }
        }

        bool songPlaying = false;
        foreach(AudioSource song in songs)
        {
            if (song.isPlaying)
                songPlaying = true;
        }
        if (!songPlaying)
        {
            if (encounterActive)
            {
                if (playBoss)
                    songs[6].Play();
                else
                    songs[2].Play();
            }
            else
            {
                int i = Random.Range(0, 2);
                songs[i].Play();
            }
        }
    }

    public void WaitForWaves(bool pause, bool bossBattle)
    {
        for (int i = 0; i < songs.Length; i++)
        {
            songs[i].Stop();
        }

        if (pause && !bossBattle) {
            //play battle theme
            playBoss = false;
            songs[2].Play();
        }
        else if (pause && bossBattle)
        {
            songs[6].Play();
            playBoss = true;
        }
        else
        {
            //play victory theme
            songs[3].Play();
            if (bossBattle)
                bossDefeated = true;
        }

        encounterActive = pause;
    }

    public void PlayGameOver()
    {
        if (!songs[4].isPlaying)
        {
            for(int i = 0; i < songs.Length; i++)
            {
                if(i != 4 && songs[i].isPlaying && i != 5)
                {
                    songPaused = i;
                    songs[i].Pause();
                }
            }
            songs[4].Play();
            gameOverPlaying = true;
        }
    }

    public void PlaySuper()
    {
        if (!songs[5].isPlaying && !superPlaying)
        {
            for (int i = 0; i < songs.Length; i++)
            {
                if (i != 5 && songs[i].isPlaying)
                {
                    if(songPaused != 4)
                        songPaused = i;
                    songs[i].Pause();
                }
            }

            songs[5].Play();
            songs[5].time = superTime;
            superPlaying = true;
            superSongTimer = 60;
        }
    }

    public bool PerformAssist()
    {
        if (!assistActive)
        {
            activeAssist = Instantiate(assists[assistI]) as GameObject;
            activeAssist.transform.position = assistSpawn.position;
            assistActive = true;
            assistOkay.SetActive(false);

            assistI++;
            if (assistI == assists.Length)
                assistI = 0;

            return true;
        }
        return false;
    }

    public bool CanCallAssist()
    {
        return !assistActive;
    }

    public void AssistDone()
    {
        //assistActive = false;
    }
}
