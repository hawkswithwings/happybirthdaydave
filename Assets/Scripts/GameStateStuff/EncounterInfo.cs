﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterInfo : MonoBehaviour {

    public bool BossWave;

    public GameObject[] enemyTypes;
    public int waves;
    public int enemiesPerWave;
    public GameStateManager manager;
    
    private Vector3[] spawnPoints;

    private int numEnemiesWave;
    private int currentWave;
    private float Left, Right, Bottom, Top;
    private int spawnBuffer;
    private int enemiesOnScreen;

    private bool spawning;
    private int lastSpawn;

    // Use this for initialization
    void Start () {
        currentWave = 0;

        Left = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x + 0.5f;
        Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;
        Bottom = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y + 1.0f;
        Top = 0.0f;

        spawnBuffer = 0;
        numEnemiesWave = 0;

        spawning = false;

        lastSpawn = -1;
    }
	
	// Update is called once per frame
	void Update () {
		if(currentWave != 0 && currentWave <= waves)
        {
            enemiesOnScreen = GameObject.FindGameObjectsWithTag("Enemy").Length;
            if(enemiesOnScreen == 0 && spawning == false && currentWave <= waves)
            {
                //move to next wave and start spawning
                currentWave++;

                //stop if currentWave is now greater than waves
                if(currentWave > waves)
                {
                    spawning = false;
                }
                else
                {
                    spawning = true;
                    numEnemiesWave = 0;
                }
            }

            if (spawnBuffer == 60 && spawning)
            {
                //randomly pick point and create enemy there
                int typeI = Random.Range(0, enemyTypes.Length);
                if (BossWave)
                {
                    typeI = 0;
                    if (lastSpawn != -1)
                    {
                        typeI = 1;
                    }
                }

                int pointI = Random.Range(0, 4);
                while(pointI == lastSpawn)
                {
                    pointI = Random.Range(0, 4);
                }
                lastSpawn = pointI;
                
                GameObject newEnemy = Instantiate(enemyTypes[typeI]) as GameObject;
                newEnemy.transform.position = spawnPoints[pointI];

                numEnemiesWave++;
                spawnBuffer = 0;
            }
            else if(spawnBuffer < 60 && spawning)
            {
                //Wait half a second to make next enemy
                spawnBuffer++;
            }

            if (numEnemiesWave == enemiesPerWave)
            {
                //stop spawning enemies
                spawning = false;
            }
        }

        if(currentWave > waves)
        {
            //Reactivate GameStateManager
            manager.WaitForWaves(false, BossWave);
            currentWave = 0;
        }
	}

    public void MakeEnemies()
    {
        manager.WaitForWaves(true, BossWave);
        currentWave = 1;
        numEnemiesWave = 0;

        Left = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x + 0.5f;
        Right = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x - 0.5f;
        Bottom = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y + 1.0f;
        Top = 0.0f;

        spawnPoints = new Vector3[] {
            new Vector3(Left, Bottom, 0.0f),
            new Vector3(Left, Top, 0.0f),
            new Vector3(Right, Bottom, 0.0f),
            new Vector3(Right, Top, 0.0f),
        };

        spawning = true;
    }

    public int GetNumWaves()
    {
        return waves;
    }

    public int GetEnemiesPerWave()
    {
        return enemiesPerWave;
    }
}
