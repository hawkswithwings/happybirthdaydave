﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Load1PLevel()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Load2PLevel()
    {
        SceneManager.LoadScene("Level2");
    }

    public void load3PLevel()
    {
        SceneManager.LoadScene("Level3");
    }

    public void load4PLevel()
    {
        SceneManager.LoadScene("Level4");
    }

    public void LoadCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void LoadTitleScreen()
    {
        SceneManager.LoadScene("TitleScreen");
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
