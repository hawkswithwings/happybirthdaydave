﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public GameObject player;
    public GameObject player2;

    public Transform[] Flags;

    private Vector3 offset;

    private bool stopped;
    private bool pointReached;
    private int currentFlag;

    private float goalX;

    // Use this for initialization
    void Start () {
        stopped = false;
        currentFlag = 0;

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if(players.Length == 2)
        {
            int i = 0;
            if(players[i].name != "Karley")
            {
                i = 1;
            }
            player2 = players[i];
        }
    }

    // Update is called once per frame
    void Update () {
        
	}

    void FixedUpdate()
    {
        goalX = player.transform.position.x;
        if (player2 != null)
        {
            Vector3 mid = (player.transform.position + player2.transform.position)/2.0f;
            goalX = mid.x;
        }


        if(!stopped && !pointReached)
        {
            Vector3 startPoint = this.transform.position;
            //Vector3 goalPoint = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
            Vector3 goalPoint = new Vector3(goalX, transform.position.y, transform.position.z);
            float step = 6.0f * Time.deltaTime;
            if (startPoint.x >= goalPoint.x)
                pointReached = true;
            else
                transform.position = Vector3.MoveTowards(startPoint, goalPoint, step);
        }

        if (!stopped && transform.position.x < goalX && pointReached)
        {
            transform.position = new Vector3(goalX, transform.position.y, transform.position.z);
        }

        /*if (!stopped && transform.position.x < player.transform.position.x && pointReached)
        {
            transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
        }*/

        if (!stopped && transform.position.x >= Flags[currentFlag].position.x)
        {
            if(Flags[currentFlag].GetComponent<EncounterInfo>() != null)
                Flags[currentFlag].GetComponent<EncounterInfo>().MakeEnemies();
            stopped = true;
        }
    }

    public void IncrementFlag()
    {
        if(currentFlag < Flags.Length-1)
            currentFlag++;
    }

    public bool AtEnd()
    {
        if (currentFlag == Flags.Length - 1)
            return true;
        else
            return false;
    }

    public bool IsStopped()
    {
        return stopped;
    }

    public void StopCamera(bool move)
    {
        stopped = move;
        pointReached = move;

        if (!stopped)
        {
            IncrementFlag();
        }
    }
}
